import jwt, datetime, os 
from flask import Flask, request
from flask_mysqldb import MySQL

server = Flask(__name__)

server.config['MYSQL_HOST'] = '127.0.0.1'
server.config['MYSQL_USER'] = 'root'
server.config['MYSQL_PASSWORD'] = 'root'
server.config['MYSQL_DB'] = 'auth'
server.config['MYSQL_PORT'] = 3306

mysql = MySQL(server)

@server.route("/login", methods=['POST'])
def login():
    auth = request.authorization
    
    if not auth:
        return 'missing credentials', 401

    cur = mysql.connection.cursor()
    res = cur.execute(f"SELECT email, password FROM user WHERE email='{auth.username}';")

    if res > 0:
        user_row = cur.fetchone()
        email = user_row[0]
        password = user_row[1]

        if auth.username != email or auth.password != password:
            return 'Credenciais inválidas', 401
        
        else:
            return createJWT(auth.username, os.environ.get("JWT_SECRET"), True)
    else:
        return 'Usuário não encontrado', 401

@server.route('/validate', methods=['POST'])
def validate():
    encoded_jwt = request.headers["Authorization"]
    
    if not encoded_jwt:
        return 'Credenciais inválidas', 401

    encoded_jwt = encoded_jwt.split(" ")[1]

    try:
        decode = jwt.decode(
            encoded_jwt, os.environ.get("JWT_SECRET"), algorithm=["HS256"]
        )
    except:
        return "Não autorizado", 403
    
    return decode, 200
    
def createJWT(username, secret, authz):
    return jwt.encode(
            {
                "username": username,
                "exp": str(datetime.datetime.now(tz=datetime.timezone.utc)
                + datetime.timedelta(days=1)),
                "iat": str(datetime.datetime.utcnow()),
                "admin": authz,
            },
            secret,
            algorithm="HS256"
    )
    
if __name__ == '__main__':
    server.run(debug=True, host="0.0.0.0", port=5000)
